import { callApi } from '../helpers/apiHelper';
import { animate } from '../fightersView';
import { callFunc } from '../helpers/domHelper';

export async function getFighters() {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id) {
  try {
    const endpoint = `details/fighter/${id}.json`;
    let details =  await callApi(endpoint, 'GET');

    return details;
  } catch (error) {
    throw error;
  }

}

export function filterFightersInDomByName(fightersDOM, fightersFind, {animCurrent = {}, animExcess = {}, animAll = {}}){

  let filtered = [];
  let all = Array.from(fightersDOM);

  if(fightersFind !== true){
    filtered = all.filter(function(item, index, array) {
      // если true - элемент добавляется к результату, и перебор продолжается
      // возвращается пустой массив в случае, если ничего не найдено
      let isRight = false;
      
      let divFighters = Array.from(item.childNodes).find( (elem) => {
        if (elem.classList.contains('name') && fightersFind.includes(elem.innerText)){
          isRight = true;
          return true;
        }});

      // is div.fighter not participating in the battle
      (!isRight) ? callFunc(animExcess, item) : callFunc(animCurrent, item);
        
      callFunc(animAll, item);

      return isRight;
    });
  } else {
    all.forEach((elem) => callFunc(animAll, elem));
  }

  return filtered;
}

export function fightersDOM() {
  return document.querySelectorAll('.fighter');
}

export function findDataFighter(fighter) {
  const progress = document.getElementById('progress').querySelector(`[data-fighter-id="${fighter._id}"]`);
  
  if (progress) {    
    return {progress, maxHealth: progress.getAttribute('data-max-health')};
  } else {
    throw new Error('The fight is over');
  }
}

