const PROGRESS_WIDTH = 450;

function calcWidthProgress(maxHealth, damage) {
    return PROGRESS_WIDTH / maxHealth * damage;
}

function calcDegreesInStr(minuend, subtrahend) {
    // minuend переводится в px 
    // изначально ~ 450px
    return parseFloat(minuend) - parseFloat(subtrahend) + "px";
} 


function backColorOnDamage(enemy, maxHealth){
    const div = 100 / maxHealth * enemy.health;
    return getColor(div);
}

function getBreakPointsProgress() {
    return {
        1: '#b00',
        15: '#b45524',
        30: '#b47024',
        45: '#c69c17',
        60: '#a8c618',
        75: '#98c618',
        90: '#3fc618'
    };
}

function getColor(div) {
    const pointsColors = getBreakPointsProgress();

    let color = "";
    for (const percent of Object.keys(pointsColors)) {
        if (div > percent) {
            color = pointsColors[percent];
        }
        else
            break;
    }
        
    return color;
}

export {getBreakPointsProgress, getColor, backColorOnDamage, calcDegreesInStr, calcWidthProgress};