import { filterFightersInDomByName, fightersDOM, findDataFighter } from "./fightersService";
import { uncheckAll, visibleProgress, style, styleMany } from "../helpers/domHelper";
import * as ps from './progressService';

export const END_FIGHT = 'endfight';
export const START_FIGHT = 'startfight';
export const ON_DAMAGE = 'ondamage';


export function initEvent(name, details = {}) {
    let event = new CustomEvent(name, {
        detail: details
    });
    return function () {
        document.dispatchEvent(event)
    };
}

export function onEndFight(event) {
    let { async } = event.detail;
    
    console.groupEnd();

    // Нужна ли де-визуализация прогресс-баров?
    // - Нет, они не были отображены
    if (async)
        visibleProgress(false);

    filterFightersInDomByName(fightersDOM(), true, {
        animAll: (elem) => {
            style(elem, false);
            style(elem.querySelector('span.checkmark'), false);
            style(elem.querySelector('img'), false);
        }
    });
    uncheckAll();
}

export function onStartFight(event) {
    const { left, right } = event.detail;

    let [figLeft, figRight] = filterFightersInDomByName(fightersDOM(), [left.name, right.name], {
        animExcess: (elem) => { elem.style.opacity = 0; }
    });
    visibleProgress(true, {
        left: {
            id: left._id,
            maxHealth: left.health
    }, right: {
            id: right._id, 
            maxHealth: right.health
    }});

    style(figLeft, {
        position: 'absolute',
        left: '20%',
        transform: 'scale(1.5)',
    });
    style(figRight, {
        position: 'absolute',
        right: '20%',
        transform: 'scale(1.5)'
    });

    style(figRight.querySelector('img'), {
        transform: 'rotateY(180deg)'
    });

    styleMany(
        figLeft.querySelector('span.checkmark'),
        figRight.querySelector('span.checkmark'),
        {display: 'none'}
    );
 
}

export function onDamage(event) {
    let {
        enemy, damage
    } = event.detail;
    // enemy - получает урон (damage)
    try {
        const { progress: enemyProg, maxHealth: enemyHealth } = findDataFighter(enemy);
        const width = getComputedStyle(enemyProg).width;
        style(enemyProg, {
            width: ps.calcDegreesInStr(width, ps.calcWidthProgress(enemyHealth, damage)),
            backgroundColor: ps.backColorOnDamage(enemy, enemyHealth)
        });

    } catch (error) {
        logError(error.message);
    }   
    
}

function logError(message) {
    console.log("%c" + message, "color: #b00; padding: 4px;");
}



