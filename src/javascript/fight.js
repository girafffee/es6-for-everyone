import { getRandomIntInclusive } from "./helpers/mathHelper";
import { initEvent, START_FIGHT, END_FIGHT, ON_DAMAGE } from "./services/eventService";

export function fight(first, second) {
  // return winner
  let firstFighter = Object.assign({}, first);
  let secondFighter = Object.assign({}, second);

  const logFight = openLogFight(firstFighter, secondFighter);  

  function repeatFirst() {
    logFight(firstFighter);
    firstFighter.health -= getDamage(secondFighter, firstFighter);
  };

  function repeatSecond() {
    logFight(secondFighter);
    secondFighter.health -= getDamage(firstFighter, secondFighter);
  };  
 
  let winner = "";

  let checkAndSend = function(repeatFirst, repeatSecond) {
    if (firstFighter.health > 0 && secondFighter.health > 0) {
      repeatFirst(); 
      repeatSecond();
      checkAndSend(repeatFirst, repeatSecond);
    } 
    else {
      winner = (firstFighter.health > secondFighter.health) ? firstFighter : secondFighter;
      initEvent(END_FIGHT, {async: false})();
    }
  };
  checkAndSend(repeatFirst, repeatSecond);

  return winner;  
}

function openLogFight(firstFighter, secondFighter){
  console.groupCollapsed(`Fight ${firstFighter.name} vs ${secondFighter.name}
  ${new Date().toLocaleTimeString()}`);

  return function (fighter) {
    console.log(`${fighter.name} - ${fighter.health}`);
  }
}

/*
Более привлекательный вид игры.
для его запуска нужно прописать в fightersView.js -> createFightersSelector():

const winner = await fightHard(...selectedFighters.values());
*/

export async function fightHard(first, second) {

  let firstFighter = Object.assign({}, first);
  let secondFighter = Object.assign({}, second);

  initEvent(START_FIGHT, {
    left: firstFighter,
    right: secondFighter
  })();

  let [timerFirst, timerSecond] = [500, 1000];  
 
  return new Promise((resolve) => {
    
    let checkAndSend = function(repeat) {
      if (firstFighter.health > 0 && secondFighter.health > 0) {
        setTimeout(repeat, timerFirst);
      } 
      else {
        const winner = (firstFighter.health > secondFighter.health) ? firstFighter : secondFighter;
        resolve(winner);
      }
    };

    setTimeout(function repeatFirst() {
      firstFighter.health -= getDamageEvent(secondFighter, firstFighter);
      checkAndSend(repeatFirst);
    }, timerFirst);

    setTimeout(function repeatSecond() {
      secondFighter.health -= getDamageEvent(firstFighter, secondFighter); 
      checkAndSend(repeatSecond);
    }, timerSecond);

  })
  .then((winner) => {
    initEvent(END_FIGHT, {async: true})();

    return winner;
  });
}

export function getDamage(attacker, enemy) {
  let damage = getHitPower(attacker) - getBlockPower(enemy);
  damage = (damage >= 0) ? damage : 0;
  
  return damage;
}

function getDamageEvent(attacker, enemy) {
  let damage = getDamage(attacker, enemy);

  initEvent(ON_DAMAGE, {enemy, damage})();
  return damage;
}

export function getHitPower(fighter) {
  return fighter.attack * getRandomIntInclusive(1, 2);
}

export function getBlockPower(fighter) {
  return fighter.defense * getRandomIntInclusive(1, 2);
}
