import { createElement, appendCreatedElements } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { attack, defense, health, name, source } = fighter;
 
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });

  appendCreatedElements(fighterDetails,
    {tagName: 'div', className: 'fighter-name', properties: {innerText: `Name: ${name}`}},
    {tagName: 'div', className: 'fighter-attack', properties: {innerText: `Attack: ${attack}`}},
    {tagName: 'div', className: 'fighter-defense', properties: {innerText: `Defense: ${defense}`}},
    {tagName: 'div', className: 'fighter-health', properties: {innerText: `Health: ${health}`}},
    {tagName: 'img', className: 'fighter-source', attributes: {src: source}},
  );

  return fighterDetails;
}
