import { showModal } from './modal';
import { createElement } from '../helpers/domHelper';

export function showWinnerModal(fighter) {
  const { name, source } = fighter;

  const title = `Winner - ${name}`;
  const bodyElement = createElement({ tagName: 'div', className: 'modal-body'});

  bodyElement.append(createElement({tagName: 'img', className: 'winner-img', attributes: {src: source}}));
  showModal({ title, bodyElement });
}