export function createElement({ tagName, className = "", attributes = {}, properties = {}}) {
  const element = document.createElement(tagName);
  
  if (className) {
    className.split(" ").forEach( (cls) => {
      element.classList.add(cls);
    });    
  }

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
  Object.keys(properties).forEach(key => element[key] = properties[key]);

  return element;
}

export function appendCreatedElements(parent, ...tags) {
  tags.map((element) => {
    parent.append(createElement(element));
  });  
}

export function callFunc(name, ...params) {
  if(typeof name === 'function')
    name(...params);
}

export function uncheckAll() {
  Array.from(document.querySelectorAll('input:checked'))
    .forEach((el) => el.checked = false);
}

export function getChildProgress() {
  return document.querySelectorAll('#progress > span > span');
}

export function getProgressAll() {
  return document.querySelectorAll('#progress span');
}

export function visibleProgress(visible = true, {left, right} = {}) {
  createProgress({left, right}); 

  Array.from(getProgressAll()).
    forEach( (el) => {
      visible ? el.style.opacity = 1 : 
        style(el, {
          opacity: 0, 
          width: "",
          backgroundColor: ""
        })
    });
}

function createProgress({left, right}) { 
  if(document.querySelector('#progress')){
    changeProgress({left, right});
    return;
  }

  let [leftFighter, rightFighter] = [getLeft(left, right), getRight(left, right)];

  const progressDiv = createElement({ tagName: 'div', attributes: {id: 'progress'} });
  const parentLeft = createElement({tagName: 'span', className: 'progress parent left'});
  const parentRight = createElement({tagName: 'span', className: 'progress parent right'});

  parentLeft.append(createElement({tagName: 'span', className: 'progress child', attributes: {
    "data-fighter-id": leftFighter.id,
    "data-max-health": leftFighter.maxHealth
  }}));
  parentRight.append(createElement({tagName: 'span', className: 'progress child', attributes: {
    "data-fighter-id": rightFighter.id,
    "data-max-health": rightFighter.maxHealth
  }}));

  progressDiv.append(parentLeft, parentRight);
  document.getElementById('root').append(progressDiv);
}


function changeProgress({left, right}) {

  let childLeft = document.querySelector('span.parent.left .child');
  let childRight = document.querySelector('span.parent.right .child');  
  
  if(left && right) {
    let [leftFighter, rightFighter] = [getLeft(left, right), getRight(left, right)];

    setAttr(childLeft, {
      'data-fighter-id': leftFighter.id,
      'data-max-health': leftFighter.maxHealth
    });

    setAttr(childRight, {
      'data-fighter-id': rightFighter.id,
      'data-max-health': rightFighter.maxHealth
    });

  } else {
    setAttrMany(childLeft, childRight, {
      'data-fighter-id': 0,
      'data-max-health': 0
    });
  }
}

function getLeft(first, second) {
  if (first === 0 && second === 0)
    return 0;
  
  return first.id < second.id ? first : second
}

function getRight(first, second) {
  if (first === 0 && second === 0)
    return 0;

  return first.id > second.id ? first : second
}

export function style(element, properties = {}) {
  if (properties) {
    Object.keys(properties)
      .forEach( (key) => element.style[key] = properties[key] );
  } else {
    element.style = "";
  }

}

export function styleMany(...elements) {
  let properties = elements.pop();
  elements.forEach( (elem) => style(elem, properties));
}

function setAttr(elem, attributes) {
  Object.keys(attributes).forEach( (attr) => elem.setAttribute(attr, attributes[attr]));
}

function setAttrMany(...elements) {
  let attributes = elements.pop();
  elements.forEach( (elem) => setAttr(elem, attributes));
}
